from odoo.tests.common import TransactionCase, tagged
import logging

_logger = logging.getLogger(__name__)
_logger.info("init")


@tagged("post_install", "ca")
class TestModelCA(TransactionCase):
    _logger.info("init2")

    def setUp(self):
        super(TestModelCA, self).setUp()

    @tagged("post_install", "-at_install", "ca")
    def test(self):
        _logger.info("    def test_logic(self):")

        rp = self.env["res.partner"]
        pt = self.env["product.template"]
        am = self.env["account.move"]
        ca = self.env["mobilunity.collective_account"]
        partner1 = rp.create({"name": "parner1"})
        partner2 = rp.create({"name": "parner2"})
        product1 = pt.create({"name": "product1", "lst_price": 1})
        product2 = pt.create({"name": "product3", "lst_price": 3})
        invs = am.create(
            [
                {
                    "move_type": "out_invoice",
                    "partner_id": partner1.id,
                    "invoice_line_ids": [
                        (
                            0,
                            None,
                            {
                                "product_id": product1.id,
                                "quantity": 3,
                                "price_unit": 1000,
                            },
                        ),
                        (
                            0,
                            None,
                            {
                                "product_id": product2.id,
                                "quantity": 1,
                                "price_unit": 3000,
                            },
                        ),
                    ],
                },
                {
                    "move_type": "out_invoice",
                    "partner_id": partner1.id,
                    "invoice_line_ids": [
                        (
                            0,
                            None,
                            {
                                "product_id": product3.id,
                                "quantity": 1,
                                "price_unit": 6000,
                            },
                        ),
                    ],
                },
                {
                    "move_type": "out_invoice",
                    "partner_id": partner2.id,
                    "invoice_line_ids": [
                        (
                            0,
                            None,
                            {
                                "product_id": product1.id,
                                "quantity": 1,
                                "price_unit": 1200,
                            },
                        ),
                    ],
                },
                {
                    "move_type": "out_invoice",
                    "partner_id": partner1.id,
                    "invoice_line_ids": [
                        (
                            0,
                            None,
                            {
                                "product_id": product2.id,
                                "quantity": 1,
                                "price_unit": 60,
                            },
                        ),
                    ],
                },
                {
                    "move_type": "out_invoice",
                    "partner_id": partner2.id,
                    "invoice_line_ids": [
                        (
                            0,
                            None,
                            {
                                "product_id": product2.id,
                                "quantity": 1,
                                "price_unit": 60,
                            },
                        ),
                    ],
                },
                {
                    "move_type": "out_invoice",
                    "partner_id": partner1.id,
                    "invoice_line_ids": [
                        (
                            0,
                            None,
                            {
                                "product_id": product3.id,
                                "quantity": 1,
                                "price_unit": 12,
                            },
                        ),
                    ],
                },
            ]
        )
        for inv in invs:
            inv.action_post
        ca11 = ca.search([("name", "=", product1.id), ("partner_id", "=", partner1.id)])
        self.assertEqual(ca11.total_product_qty, 3)
        self.assertEqual(ca11.total_product_price, 1000)
        ca12 = ca.search([("name", "=", product1.id), ("partner_id", "=", partner2.id)])
        self.assertEqual(ca12.total_product_qty, 1)
        self.assertEqual(ca12.total_product_price, 1200)
        ca21 = ca.search([("name", "=", product2.id), ("partner_id", "=", partner1.id)])
        self.assertEqual(ca21.total_product_qty, 2)
        self.assertEqual(ca21.total_product_price, 3060)
        ca22 = ca.search([("name", "=", product2.id), ("partner_id", "=", partner2.id)])
        self.assertEqual(ca22.total_product_qty, 1)
        self.assertEqual(ca22.total_product_price, 60)

