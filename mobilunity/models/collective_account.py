from odoo import api, fields, models


class ColectiveAccount(models.Model):
    _name = "mobilunity.collective_account"
    _description = "Test task Odoo"

    name = fields.Many2one(comodel_name="product.template", string="Product name")
    currency_id = fields.Many2one(
        "res.currency",
        string="Currency",
        default=lambda self: self.env.user.company_id.currency_id,
    )
    total_product_qty = fields.Integer(string="Total Product QTY")
    total_product_price = fields.Monetary(
        string="Total Product Price", currency_field="currency_id",
    )
    partner_id = fields.Many2one("res.partner", string="Partner ID")


class AccountCollective(models.Model):
    _inherit = "account.move"
    _description = "count account move"

    def action_post(self):
        origin = super(AccountCollective, self).action_post()
        for line in self.invoice_line_ids:
            if line.product_id.type == "consu":
                mc = self.env["mobilunity.collective_account"]
                exist = mc.search(
                    [
                        ("name", "=", line.product_id.id),
                        ("partner_id", "=", self.partner_id.id),
                    ]
                )
                if exist:
                    exist.total_product_qty += line.quantity
                    exist.total_product_price += line.price_subtotal
                else:
                    mc.create(
                        {
                            "name": line.product_id.id,
                            "partner_id": self.partner_id.id,
                            "total_product_qty": line.quantity,
                            "total_product_price": line.price_subtotal,
                        }
                    )
        return origin
